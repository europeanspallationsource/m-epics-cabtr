EXCLUDE_VERSIONS=3.14
include ${EPICS_ENV_PATH}/module.Makefile

DOC       = README.md
EXECUTABLES = src/cabtr-calib.py src/cabtr_dump_dir.py src/cabtr-calib-pyepics.py src/cabtr_channel_config.py
USR_DEPENDENCIES += modbus,2.9.0-ESS2+
USR_DEPENDENCIES += synappsstd
