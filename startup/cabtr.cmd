#@field DEVICENAME
#@type STRING
# Device name, should be the same as the name in CCDB

#@field IPADDR
#@type STRING
# Device IP address

#@field POLL
#@type INTEGER
# Read poll in msec

#@field REQUIRE_cabtr_PATH
#@runtime YES

#@field EPICSVERSION
#@runtime YES

#@field ARCH
#@runtime YES

#@field PYTHON3
#@runtime YES

#@field MB_PORT
#@runtime YES

#@field WB_PORT
#@runtime YES



#-######### Set up comms driver ##########
#-Specify the TCP endpoint and give your bus a name eg. "PLC1".
#-drvAsynIPPortConfigure(portName, hostInfo, priority, noAutoConnect, noProcessEos)
#-where the arguments are:
#-      portName - The portName that is registered with asynManager.
#-      hostInfo - The Internet host name, port number and optional IP protocol of the device (e.g.
#-                              "164.54.9.90:4002", "serials8n3:4002", "serials8n3:4002 TCP" or "164.54.17.43:5186 udp"). If no
#-                              protocol is specified, TCP will be used. Possible protocols are:
#-              TCP,
#-              UDP,
#-              UDP* — UDP broadcasts. The address portion of the argument must be the network broadcast address
#-                              (e.g. "192.168.1.255:1234 UDP*").
#-              HTTP — Like TCP but for servers which close the connection after each transaction.
#-              COM — For Ethernet/Serial adapters which use the TELNET RFC 2217 protocol. This allows port parameters
#-                              (speed, parity, etc.) to be set with subsequent asynSetOption commands just as for local serial
#-                              ports. The default parameters are 9600-8-N-1 with no flow control.
#-      priority - Priority at which the asyn I/O thread will run. If this is zero or missing, then
#-      epicsThreadPriorityMedium is used.
#-      noAutoConnect - Zero or missing indicates that portThread should automatically connect. Non-zero if explicit
#-                                      connect command must be issued.
#-      noProcessEos - If 0 then asynInterposeEosConfig is called specifying both processEosIn and processEosOut.
drvAsynIPPortConfigure("$(DEVICENAME)", "$(IPADDR):$(MB_PORT=502)", 0, 0, 1)

#-Configure the interpose for TCP
modbusInterposeConfig("$(DEVICENAME)", 0, 120000, 0)


#-Configure the Modbus driver
#-drvModbusAsynConfigure( portName,               tcpPortName,        slaveAddress,  modbusFunction,  modbusStartAddress,   modbusLength,    dataType,  pollMsec,    plcType);
drvModbusAsynConfigure(   "$(DEVICENAME)-R0",     "$(DEVICENAME)",    0,             4,                0,                   73,              0,         $(POLL),     "CABTR")

#-Resistance and temperature measurements
#-drvModbusAsynConfigure(   "$(DEVICENAME)-R256",   "$(DEVICENAME)",    0,             4,                256,                 32,              0,         $(POLL),     "CABTR")

#-CABTR and measurement state
drvModbusAsynConfigure(   "$(DEVICENAME)-R288",   "$(DEVICENAME)",    0,             4,                288,                 16,              0,         $(POLL),     "CABTR")

#-Chan 1-4
drvModbusAsynConfigure(   "$(DEVICENAME)-R304",   "$(DEVICENAME)",    0,             4,                304,                 120,             0,         $(POLL),     "CABTR")
drvModbusAsynConfigure(   "$(DEVICENAME)-W304",   "$(DEVICENAME)",    0,             16,               304,                 120,             0,         0,           "CABTR")
#-Chan 5-8
drvModbusAsynConfigure(   "$(DEVICENAME)-R424",   "$(DEVICENAME)",    0,             4,                424,                 120,             0,         $(POLL),     "CABTR")
drvModbusAsynConfigure(   "$(DEVICENAME)-W424",   "$(DEVICENAME)",    0,             16,               424,                 120,             0,         0,           "CABTR")

drvModbusAsynConfigure(   "$(DEVICENAME)-RQ",     "$(DEVICENAME)",    0,             4,                768,                 120,             0,         $(POLL),     "CABTR")


#-General write port
drvModbusAsynConfigure(   "$(DEVICENAME)-W",      "$(DEVICENAME)",    0,             16,              -1,                   8,               0,         0,           "CABTR")

#-Load the database defining your EPICS records
dbLoadRecords(cabtr.template,  "P=$(DEVICENAME), R=:, W_TIMEOUT = 500, ASYNPORT = $(DEVICENAME)")
dbLoadRecords(cabtr_chan.db,   "P=$(DEVICENAME), R=:, W_TIMEOUT = 500, ASYNPORT = $(DEVICENAME)")
dbLoadRecords(cabtr_commit.db, "P=$(DEVICENAME), R=:, W_TIMEOUT = 500, ASYNPORT = $(DEVICENAME)")

epicsEnvSet PYTHON3 $(PYTHON3=python3)
doAfterIocInit("system('${PYTHON3} ${REQUIRE_cabtr_PATH}/${EPICSVERSION}/bin/${ARCH}/cabtr-calib-pyepics.py ${DEVICENAME} : ${IPADDR}:${WB_PORT=80}&')")
