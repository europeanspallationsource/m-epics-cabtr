#!/usr/bin/env python3

from p4p.server import Server, StaticProvider
from p4p.nt import NTScalar
from p4p.server.thread import SharedPV
from cabtr_dump_dir import dump_dir


class Cabtr(StaticProvider):
    def __init__(self, p, r, ip):
        super(Cabtr, self).__init__("cabtr-{}".format(p))

        self.ip       = ip
        self.prefix   = p
        self.filesPV  = SharedPV(nt = NTScalar("as"))
        self.rereadPV = SharedPV(handler = self, nt = NTScalar("b"), initial = 0)
        self.add("{}{}CalibFiles-RB".format(p, r),  self.filesPV)
        self.add("{}{}CalibRereadCmd".format(p, r), self.rereadPV)

        self.filesPV.open(self.filesPV._wrap({"value":[], "alarm.severity":3}))
        self.reread()


    def __reread(self):
        value = dump_dir(self.ip)
        self.filesPV.post(self.filesPV._wrap({"value":value, "alarm.severity":0}))
        self.filesPV.post(value)


    def reread(self):
        print("Rereading...")
        try:
            self.__reread()
            print("...done")
        except Exception as e:
            print("{} @ {}:".format(self.prefix, self.ip), e)
            val = self.filesPV._wrap({"value":[], "alarm.severity":3})
            self.filesPV.post(val)
            self.filesPV.post([])


    def put(self, pv, op):
        if pv != self.rereadPV:
            op.done(error="Not supported")

        pv.post(op.value().real)
        op.done()
        self.reread()


cabtr = Cabtr("cabtr", ":", ip = "127.0.0.1:8888")
Server.forever(providers=[cabtr])
