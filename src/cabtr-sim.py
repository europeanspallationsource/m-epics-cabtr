#!/usr/bin/env python3

from __future__ import absolute_import
from __future__ import print_function

from time import sleep
from threading import Thread

import pymodbus.server.sync as modbus
from pymodbus.datastore.store import ModbusSequentialDataBlock
from pymodbus.datastore.context import ModbusServerContext, ModbusSlaveContext

from http import HTTPStatus as HTTPStatus
from http.server import SimpleHTTPRequestHandler as SimpleHTTPRequestHandler
from http.server import HTTPServer as HTTPServer


class CabtrHTTPRequestHandler(SimpleHTTPRequestHandler):
    def do_HEAD(self):
        if self.path.endswith("/lin/dump_dir.txt"):
            return self.send_head(self.__dump_dir_txt())
        elif self.path.endswith("/config/init.txt"):
            return self.send_head(self.__init_txt())
        else:
            raise RuntimeError("Unknown path: " + self.path)


    def do_GET(self):
        c = self.do_HEAD()
        self.wfile.write(c.encode())
        self.wfile.flush()


    def log_request(self, code):
        pass


    def __init_txt(self):
        dd = []
        for ch in self.server.cabtr_channels():
            dd.append("Nom voie{} : {}".format(ch._idx, ch.name))
            dd.append("Fichier voie{} : {}".format(ch._idx, ch.curve))

        return """
""".join(dd)


    def __dump_dir_txt(self):
        dd = set()
        for ch in self.server.cabtr_channels():
            dd.add("{} 420".format(ch.curve))

        dd = list(dd)
        dd.append("[USED (ko)] : 690")
        dd.append("[FREE (ko)] : 690")

        return """
""".join(dd)


    def send_head(self, content):
        try:
            self.send_response(HTTPStatus.OK)
            self.send_header("Content-type", "text/html; charset=UTF-8;")
            self.send_header("Content-Length", len(content))
            self.send_header("Last-Modified", self.date_time_string())
            self.end_headers()
            return content
        except:
            raise


class Channel(object):
    def __init__(self, mb, idx, name, curve):
        self.__mb      = mb
        self._idx      = idx
        self.__address = 304 + 30 * (idx - 1)
        self.name      = name
        self.curve     = curve
        self.__prog    = 0

        self.__name_a  = self.__address + 0
        self.__curve_a = self.__address + 8
        self.__prog_a  = self.__address + 24

        self.mb_name  = self.name
        self.mb_curve = self.curve


    def __str__(self):
        return """Channel{idx} // {addr}
\tName : {n} ({mn})
\tCurve: {c} ({mc})
\tProg : {p}
""".format(idx = self._idx, n = self.name, mn = self.mb_name, c = self.curve, mc = self.mb_curve, p = self.__prog, addr = self.__address)


    def is_this(self, address):
        if self.__address <= address and address < self.__address + 30:
            return True

        return False


    def commit(self):
        if self.__prog & 1:
            self.name   = self.mb_name
            pass
        if self.__prog & 2:
            self.curve  = self.mb_curve
            pass
        if self.__prog:
            self.__prog = 0
            print(self)
            self.serialize()


    def deserialize(self, address, values):
        if not self.is_this(address):
            print("Address does not belong to channel")
            raise RuntimeError("Address does not belong to channel")

        if address == self.__name_a:
            self.mb_name = Cabtr.deserialize(values, str)
        elif address == self.__curve_a:
            self.mb_curve = Cabtr.deserialize(values, str)
        elif address == self.__prog_a:
            self.__prog = Cabtr.deserialize(values, int)
        else:
            print("Not writeable address", address)
            raise RuntimeError("Not writeable address", address)

        # Commit our state
        self.serialize()
        print(self)


    def serialize(self):
        self.__mb.direct_setValues(self.__name_a,  Cabtr.serialize(self.mb_name, str, 8))
        self.__mb.direct_setValues(self.__curve_a, Cabtr.serialize(self.mb_curve, str, 16))
        self.__mb.direct_setValues(self.__prog_a,  Cabtr.serialize(self.__prog, int, 1))



import time
class Cabtr(ModbusSequentialDataBlock):
    COMMIT_CHANNELS = 57
    SAVE_CHANGES    = 53

    def __init__(self, mb_port = 502, web_port = 80):
        super(Cabtr, self).__init__(0, [0x0] * 900)

        self.mb_port  = mb_port
        self.web_port = web_port

        self.channels = [
                            Channel(self, 1, "TT69", "pt100.pol"),
                            Channel(self, 2, "TT23", "pt100.pol"),
                            Channel(self, 3, "TT91", "TT91X107576.cof"),
                            Channel(self, 4, "TT92", "TT92X107575.cof"),
                            Channel(self, 5, "TT93", "TT93X107574.cof"),
                            Channel(self, 6, "TT94", "TT94X107573.cof"),
                            Channel(self, 7, "TT30", "TT30X107512.cof"),
                            Channel(self, 8, "TT49", "TT30X107512.cof"),
                        ]

        self.__mb_server = self.__create_modbus()
        self.__wb_server = self.__create_web()

        # Model
        self.direct_setValues(0,  self.serialize("CABTR", str, 4))
        self.direct_setValues(24, self.serialize("1.0", str, 4))
        # Test values
        self.direct_setValues(58, self.serialize(0x1234, int, 1))
        self.direct_setValues(59, self.serialize(0x1A2B3C4D, int, 2))
        self.direct_setValues(61, self.serialize(0x47F12065, int, 2))

        for ch in self.channels:
            ch.serialize()

        print("""CABTR Initialized
""")
        for ch in self.channels:
            print(ch)


    @staticmethod
    def serialize(values, typ = int, count = 1):
        try:
            if typ is int:
                if count == 1:
                    values = [ values ]
                elif count == 2:
                    values = [values & 0xFFFF, values >> 16]
                else:
                    raise RuntimeError("Cannot serialize", values)
            elif typ is str:
                tmp = values
                values = [0] * count
                count = len(tmp)
                for x in range(0, count // 2):
                    values[x] = (ord(tmp[x * 2 + 1]) << 8) | ord(tmp[x * 2])
                if count % 2:
                    # Zero based, no need to add + 1
                    values[count // 2] = ord(tmp[-1])
            else:
                raise RuntimeError("Cannot deserialize unknown type", typ)
        except Exception as e:
            print(e)
            raise

        return values


    @staticmethod
    def deserialize(values, typ):
        try:
            if typ is int:
                if len(values) == 1:
                    ret = values[0]
                elif len(values) == 2:
                    ret = (values[1] << 16) | values[0]
                else:
                    raise RuntimeError("Cannot deserialize", values)
            elif typ is str:
                ret = []
                for v in values:
                    ret.append(chr(v & 0xFF))
                    ret.append(chr(v >> 8))
                ret = "".join(ret)
            else:
                raise RuntimeError("Cannot deserialize unknown type", typ)
        except Exception as e:
            print(e)
            raise

        print("Deserialized to ", ret)
        return ret


#    def getValues(self, address, count = 1):
#        return super(Cabtr, self).getValues(address, count)


    def direct_setValues(self, address, values):
        return super(Cabtr, self).setValues(address, values)


    def setValues(self, address, values):
        print("setValues", address, values)

        for ch in self.channels:
            if ch.is_this(address):
                ch.deserialize(address, values)
                return

        # Channel related writes are done by the channel object
        super(Cabtr, self).setValues(address, values)

        if values[0] == 1:
            if address == self.COMMIT_CHANNELS:
                print("Committing channel changes...")
                for ch in self.channels:
                    ch.commit()
                v = self.getValues(32, 1)[0]
                v = v | 64
                self.direct_setValues(32, v)

                #FIXME: check if we have to reset COMMIT_CHANNELS
            elif address == self.SAVE_CHANGES:
                print("Saving changes...")
                v = self.getValues(32, 1)[0]
                v = v & ~64
                self.direct_setValues(32, v)


    def run(self):
        mb_thread = Thread(name = "Modbus", target = self.__mb_server.serve_forever)
        mb_thread.start()

        wb_thread = Thread(name = "Web", target = self.__wb_server.serve_forever)
        wb_thread.start()

        try:
            while True:
                sleep(10)
        except:
            pass

        self.__mb_server.shutdown()
        self.__wb_server.shutdown()
        mb_thread.join()
        wb_thread.join()


    def __create_modbus(self):
        slave = ModbusSlaveContext(ir = self, hr = self, di = self, co = self, zero_mode = True)
        ctx = ModbusServerContext(slaves = slave, single = True)
        return modbus.ModbusTcpServer(context = ctx, address=("localhost", self.mb_port))


    def __create_web(self):
        class CabtrHTTPServer(HTTPServer):
            def __init__(self, cabtr, *args, **kw_args):
                super(CabtrHTTPServer, self).__init__(*args, **kw_args)
                self.__cabtr = cabtr

            def cabtr_channels(self):
                return self.__cabtr.channels

        return CabtrHTTPServer(self, ("localhost", self.web_port), CabtrHTTPRequestHandler)



if __name__ == "__main__":
    Cabtr(5020, 8888).run()
