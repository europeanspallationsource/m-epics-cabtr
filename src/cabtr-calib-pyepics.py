#!/usr/bin env python3

from sys import argv, stderr
import epics
from cabtr_dump_dir import dump_dir
from cabtr_channel_config import channel_config

do_loop = True

class Cabtr(epics.Device):
    FREREAD   = "iCalibReread"
    FILES     = "CalibFiles-RB"

    CH_REREAD = "iChannelReread"
    CH_NAME   = "Chan{}NameR"
    CH_CALIB  = "Chan{}CurveFileR"

    def __init__(self, p, r, ip):
        super(Cabtr, self).__init__(prefix = p, delim = r, nonpvs = [ "_ip" ])

        self._ip = ip
        # Connect now
        self.PV(self.FILES, connection_callback = self.conn_callback)
        for i in range(1, 9):
            self.PV(self.CH_NAME.format(i), connection_callback = self.conn_callback)
            self.PV(self.CH_CALIB.format(i), connection_callback = self.conn_callback)

        # This MUST be after creating self.FILES or the connection callback will not be set
        self.add_callback(self.FREREAD, self.reread_file_list, run_now = True)
        # This MUST be after creating self.CH_NAMEs and self.CH_CALIBs or the connection callback will not be set
        self.add_callback(self.CH_REREAD, self.reread_ch_config, run_now = True)


    def __reread_file_list(self):
        return dump_dir(self._ip)


    def __reread_ch_config(self):
        return channel_config(self._ip)


    def ch_name(self, i):
        return self.PV(self.CH_NAME.format(i))


    def ch_curve_file(self, i):
        return self.PV(self.CH_CALIB.format(i))


    def conn_callback(self, pvname, conn, pv):
#        print("conn_callback", pvname, conn, pv)
        if not conn:
            global do_loop
            do_loop = False


    def reread_file_list(self, **keyword_params):
#        print("Rereading...")
        if self.FREREAD not in keyword_params.get('pvname', ""):
            return
        try:
            values = [x.decode() for x in self.__reread_file_list()]
#            print("...caputting...")
            self.PV(self.FILES).put(values)
#            print("...done")
        except Exception as e:
            print("{} @ {}:".format(self._prefix, self._ip), e, file = stderr)
            self.put(self.FILES, [''])


    def reread_ch_config(self, **keyword_params):
        if self.CH_REREAD not in keyword_params.get('pvname', ""):
            return
        try:
            (names, calibs) = self.__reread_ch_config()
            for i in range(1, 9):
                try:
                    n = names[i]
                except IndexError:
                    n = ""
                self.ch_name(i).put(n)

                try:
                    c = calibs[i]
                except IndexError:
                    c = ""
                self.ch_curve_file(i).put(c)
        except Exception as e:
            print("{} @ {}:".format(self._prefix, self._ip), e, file = stderr)


print("Initializing calibration helper for {} @ {}...".format(argv[1], argv[3]), file = stderr)
Cabtr(argv[1], argv[2], argv[3])

while do_loop:
    epics.poll(evt = 1.e-2)
