import requests

def dump_dir(ip):
    res = requests.get("http://{}/lin/dump_dir.txt".format(ip))
    if res.status_code != 200:
        raise RuntimeError("Status code:" + str(res.status_code))

    value = []
    for l in res.iter_lines():
        items = l.rsplit(b" ")
        if len(items) > 2:
            continue
        value.append(items[0])

    return value
