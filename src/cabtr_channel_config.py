import requests

ch_name  = "Nom voie"
l_ch_name = len(ch_name)
ch_calib = "Fichier voie"
l_ch_calib = len(ch_calib)

def channel_config(ip):
    res = requests.get("http://{}/config/init.txt".format(ip))
    if res.status_code != 200:
        raise RuntimeError("Status code:" + str(res.status_code))

    names  = dict()
    calibs = dict()
    for l in res.iter_lines():
        items = l.rsplit(b":")
        if len(items) != 2:
            continue

        header = items[0].decode().strip()
        value  = items[1].decode().strip()
        if header.startswith(ch_name):
            x = int(header[l_ch_name:])
            names[x] = value
        elif header.startswith(ch_calib):
            x = int(header[l_ch_calib:])
            calibs[x] = value

    return (names, calibs)
